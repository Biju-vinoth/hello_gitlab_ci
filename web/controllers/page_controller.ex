defmodule HelloGitlabCi.PageController do
  use HelloGitlabCi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
